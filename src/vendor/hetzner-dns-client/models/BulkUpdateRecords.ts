import ErrorModel from './ErrorModel';
import RecordModel from './RecordModel';
import BaseRecordModel from './BaseRecordModel';

/**
 * Bulk update records response model
 * @interface
 * @extends {ErrorModel}
 */
export default interface BulkUpdateRecords extends ErrorModel {
  failed_records: BaseRecordModel[];
  records: RecordModel[];
}
