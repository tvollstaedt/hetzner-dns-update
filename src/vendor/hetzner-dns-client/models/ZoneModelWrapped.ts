import ErrorModel from './ErrorModel';
import ZoneModel from './ZoneModel';

/**
 * Hetzner DNS zone model
 * @interface
 * @extends {ErrorModel}
 */
export default interface ZoneModelWrapped extends ErrorModel {
  zone: ZoneModel;
}
