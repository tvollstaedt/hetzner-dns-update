import ErrorModel from './ErrorModel';
import RecordModel from './RecordModel';

/**
 * Hetzner DNS zone model
 * @interface
 * @extends {ErrorModel}
 */
export default interface Records extends ErrorModel {
  records: RecordModel[];
}
