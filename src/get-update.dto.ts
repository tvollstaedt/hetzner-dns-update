import { IsIP } from 'class-validator';

export class UpdateParams {
  @IsIP()
  ip: string;
}
