import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { DnsService } from './dns.service';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [DnsService],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(appController.updateDnsRecords()).toBe('Hello World!');
    });
  });
});
