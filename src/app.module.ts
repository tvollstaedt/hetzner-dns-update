import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { DnsService } from './dns.service';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [ConfigModule.forRoot({ cache: true }), AuthModule],
  controllers: [AppController],
  providers: [DnsService],
})
export class AppModule {}
