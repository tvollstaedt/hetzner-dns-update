import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { catchError, delay, from, mergeMap, throwError } from 'rxjs';
import HetznerDnsClient from './vendor/hetzner-dns-client/HetznerDnsClient';

@Injectable()
export class DnsService {
  public hetznerClient: HetznerDnsClient;

  public recordIds: string[];
  private readonly logger = new Logger(DnsService.name);

  public constructor(private configService: ConfigService) {
    const apiKey = configService.get<string>('HETZNER_API_TOKEN');
    if (!apiKey) {
      throw new Error('Hetzner API key not found. Bailing out.');
    }
    const recordIds = configService.get<string>('RECORD_IDS');
    if (!recordIds || recordIds.split(' ').length === 0) {
      throw new Error('No zone ids are provided. Bailing out.');
    }
    this.recordIds = recordIds.split(' ');
    this.hetznerClient = new HetznerDnsClient(apiKey);
  }
  public update(newIp: string) {
    this.logger.log(`Updating records to IP ${newIp}`);
    return from(this.recordIds).pipe(
      catchError((err) => {
        this.logger.log(
          `Error occured while updating Hetzner DNS records: `,
          err,
        );
        return throwError(() => new Error());
      }),
      mergeMap((recordId) => this.hetznerClient.records.get(recordId)),
      mergeMap((record) => {
        this.logger.debug(
          `Updating record ${record.id} with name ${record.name} to value ${newIp}`,
        );
        return record.update({
          value: newIp,
          ttl: this.configService.get<number>('HETZNER_RECORD_TTL', 300),
        });
      }),
      delay(500), // Throttle Hetzner API calls
    );
  }
}
