import { Controller, Get, Query } from '@nestjs/common';
import { DnsService } from './dns.service';
import { map } from 'rxjs';
import { UpdateParams } from './get-update.dto';

@Controller()
export class AppController {
  public constructor(private dnsService: DnsService) {}

  @Get()
  index() {
    return "I'm alive! See https://gitlab.com/tvollstaedt/hetzner-dns-update for more info about this project.";
  }

  @Get('update')
  updateDnsRecords(@Query() param: UpdateParams) {
    return this.dnsService.update(param.ip).pipe(map(() => 'success'));
  }
}
